﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SubSonic.Demo.Models;
using SubSonic.Repository;
using SubSonic.DataProviders;
using SubSonic.Query;

namespace SubSonic.Demo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ProviderFactory.GetProvider().MigrationToDataBase(typeof(Models.Customer), typeof(Models.ImageFile).Assembly);
                MessageBox.Show("Success!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            try
            {

                var customer = new Customer() { Name = "Moisés Miranda" };

                var simpleRepository = new SimpleRepository();
                simpleRepository.Add<Customer>(customer);
                MessageBox.Show("Success!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonList_Click(object sender, EventArgs e)
        {
            try
            {

                var simpleRepository = new SimpleRepository();
                dataGridView1.DataSource = simpleRepository.All<Customer>().ToList();
                MessageBox.Show("Success!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonListWithSelect_Click(object sender, EventArgs e)
        {
            try
            {
                var tableName = SubSonic.DataProviders.ProviderFactory.GetProvider().Client== DataClient.PostgreSql? "\"Customers\"": "Customers";
                var select = new Select().From(tableName);
                dataGridView2.DataSource = select.ExecuteTypedList<Customer>();
                MessageBox.Show("Success!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                var simpleRepository = new SimpleRepository();
                var firstCustomer = simpleRepository.Find<Customer>(c=>c.Id==1).LastOrDefault();
                if (firstCustomer != null)
                {
                    firstCustomer.Name = "Moisés J. Miranda";
                    simpleRepository.Update<Customer>(firstCustomer);
                }
                MessageBox.Show("Success!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                var simpleRepository = new SimpleRepository();
                var customers = simpleRepository.All<Customer>().ToList();
                var lastCustomer = customers.LastOrDefault();
                if (lastCustomer != null)
                {
                    simpleRepository.Delete<Customer>(lastCustomer.Id);
                }
                MessageBox.Show("Success!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

    }
}
