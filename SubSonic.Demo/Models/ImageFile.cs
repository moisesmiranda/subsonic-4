﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SubSonic.Demo.Models
{
    public class ImageFile
    {
        public int Id { get; set; }
        public byte[] MyProperty { get; set; }
        public byte[] ImgFile { get; set; }

    }
}
