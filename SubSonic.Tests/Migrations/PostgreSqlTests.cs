﻿using SubSonic.DataProviders;
using SubSonic.Extensions;
using SubSonic.Schema;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace SubSonic.Tests.Migrations
{
    public class PostgreSqlTests
    {
        private IDataProvider _provider;
        private Migrator migrator;

        public PostgreSqlTests()
        {
            _provider = ProviderFactory.GetProvider(TestConfiguration.PostgreSqlConnectionString, DbClientTypeName.PostgreSql);
            migrator = new Migrator(Assembly.GetExecutingAssembly());
        }


        [Fact]
        public void CreateTable_Should_CreateValid_SQL_For_SubSonicTest_Defaulted_To_Inno_UTF8()
        {
            var shouldbe =
                @"CREATE TABLE `SubSonicTests` (
  `Key` binary(16) PRIMARY KEY  NOT NULL ,
  `Thinger` int NOT NULL ,
  `Name` nvarchar(255) NOT NULL ,
  `UserName` nvarchar(500) NOT NULL ,
  `CreatedOn` datetime NOT NULL ,
  `Price` decimal(10, 2) NOT NULL ,
  `Discount` float(10, 2) NOT NULL ,
  `Lat` decimal(10, 3),
  `Long` decimal(10, 3),
  `SomeFlag` tinyint NOT NULL ,
  `SomeNullableFlag` tinyint,
  `LongText` LONGTEXT  NOT NULL ,
  `MediumText` nvarchar(800) NOT NULL  
) 
ENGINE=InnoDB DEFAULT CHARSET=utf8";

            var sql = typeof(SubSonicTest).ToSchemaTable(_provider).CreateSql;
            Assert.Equal(shouldbe, sql);
        }

        [Fact]
        public void DropTable_Should_Create_Valid_Sql()
        {
            var shouldbe = "DROP TABLE `SubSonicTests`;".Replace("`", "\"");

            var sql = typeof(SubSonicTest).ToSchemaTable(_provider).DropSql.Trim();
            Assert.Equal(shouldbe, sql);
        }

        [Fact]
        public void DropColumnSql_Should_Create_Valid_Sql()
        {
            var shouldbe = "ALTER TABLE `SubSonicTests` DROP COLUMN `UserName`;".Replace("`", "\"");

            var sql = typeof(SubSonicTest).ToSchemaTable(_provider).DropColumnSql("UserName");
            Assert.Equal(shouldbe, sql);
        }

        [Fact]
        public void CreateColumnSql_Should_Create_Valid_Sql()
        {
            var shouldbe = @"ALTER TABLE `SubSonicTests` ADD `UserName` nvarchar(500) NOT NULL  DEFAULT '';
UPDATE SubSonicTests SET UserName='';".Replace("`", "\"");

            var sql = typeof(SubSonicTest).ToSchemaTable(_provider).GetColumn("UserName").CreateSql;
            Assert.Equal(shouldbe, sql);
        }

        [Fact]
        public void AlterColumnSql_Should_Create_Valid_Sql()
        {
            var shouldbe = "ALTER TABLE `SubSonicTests` `UserName` nvarchar(500) NOT NULL ;".Replace("`", "\"");

            var sql = typeof(SubSonicTest).ToSchemaTable(_provider).GetColumn("UserName").AlterSql;
            Assert.Equal(shouldbe, sql);
        }
    }
}
