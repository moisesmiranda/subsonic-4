﻿using SubSonic.Extensions;
using SubSonic.Schema;
using SubSonic.SqlGeneration.Schema;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace SubSonic.SqlGeneration.Schema
{
    public class PostgreSqlSchema : ANSISchemaGenerator
    {
        public PostgreSqlSchema()
        {
            ADD_COLUMN = "ALTER TABLE \"{0}\" ADD \"{1}\"{2};";
            ALTER_COLUMN = "ALTER TABLE {0} ALTER COLUMN \"{1}\" TYPE {2};";
            CREATE_TABLE = "CREATE TABLE \"{0}\" ({1} \r\n) ";
            DROP_COLUMN = "ALTER TABLE \"{0}\" DROP COLUMN \"{1}\";";
            DROP_TABLE = @"DROP TABLE {0};";

        }

        /// <summary>
        /// Builds a CREATE TABLE statement.
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public override string BuildCreateTableStatement(ITable table)
        {
            string result = base.BuildCreateTableStatement(table);

            result += "\r\n TABLESPACE pg_default;";
            return result;
        }

        public override string BuildAlterColumnStatement(IColumn column)
        {
            var stringAlterColumn  =new StringBuilder( string.Format(ALTER_COLUMN, column.Table.QualifiedName, column.Name, 
                column.IsString ? $" {GetNativeType(column.DataType)} ({column.MaxLength})" :  GetNativeType(column.DataType)  ));            

            return stringAlterColumn.ToString();
        }

        /// <summary>
        /// Generates the columns.
        /// </summary>
        /// <param name="table">Table containing the columns.</param>
        /// <returns>
        /// SQL fragment representing the supplied columns.
        /// </returns>
        public override string GenerateColumns(ITable table)
        {
            StringBuilder createSql = new StringBuilder();

            foreach (IColumn col in table.Columns)
                createSql.AppendFormat("\r\n  \"{0}\"{1},", col.Name, GenerateColumnAttributes(col));
            string columnSql = createSql.ToString();
            return columnSql.Chop(",");
        }

        public override string GenerateColumnAttributes(IColumn column)
        {
            StringBuilder sb = new StringBuilder();
            if (column.DataType == DbType.Guid)
                column.MaxLength = 16;

            if (column.DataType == DbType.String && column.MaxLength > 8000)
                sb.Append(" text ");
            else
            {
                sb.Append(" " + GetNativeType(column.DataType));

                if (column.MaxLength > 0)
                    sb.Append("(" + column.MaxLength + ")");

                if (column.DataType == DbType.Double || column.DataType == DbType.Decimal)
                    sb.Append("(" + column.NumericPrecision + ", " + column.NumberScale + ")");
            }
            if (column.IsPrimaryKey)
                sb.Append(" PRIMARY KEY ");

            if (column.IsPrimaryKey | !column.IsNullable)
                sb.Append(" NOT NULL ");

            if (column.IsPrimaryKey && column.IsNumeric)
                sb.AppendFormat(" DEFAULT nextval('\"{0}\"'::regclass) ", BuildSequenceGenerator(column));

          

            return sb.ToString();
        }

        public override DbType GetDbType(string sqlType)
        {
            switch (sqlType.ToLowerInvariant())
            {
                case "longtext":
                case "nchar":
                case "ntext":
                case "text":
                case "sysname":
                case "varchar":
                case "character":
                    return DbType.String;
                case "bit":
                case "boolean":
                    return DbType.Boolean;
                case "decimal":
                case "float":
                case "newdecimal":
                case "numeric":
                case "double":
                case "real":
                    return DbType.Decimal;
                case "bigint":
                    return DbType.Int64;
                case "int":
                    return DbType.Int32;
                case "int32":
                case "integer":
                    return DbType.Int32;
                case "int16":
                case "smallint":
                    return DbType.Int16;
                case "date":
                case "time":
                case "datetime":
                case "smalldatetime":
                    return DbType.DateTime;
                case "image":
                case "varbinary":
                case "binary":
                case "blob":
                case "bytea":
                    return DbType.Binary;
                case "char":
                    return DbType.AnsiStringFixedLength;
                case "currency":
                case "money":
                    return DbType.Decimal;
                case "smallmoney":
                    return DbType.Currency;
                case "timestamp":
                    return DbType.DateTime;
                case "uniqueidentifier":                    
                case "uint16":
                    return DbType.UInt16;
                case "uint32":
                    return DbType.UInt32;
                default:
                    return DbType.String;
            }
        }

        public override string GetNativeType(DbType dbType)
        {
            switch (dbType)
            {
                case DbType.Object:
                case DbType.AnsiString:
                case DbType.AnsiStringFixedLength:
                case DbType.String:
                    return "character";
                case DbType.StringFixedLength:
                    return "varchar";
                case DbType.Boolean:
                    return "boolean";
                case DbType.SByte:
                case DbType.Binary:
                case DbType.Byte:
                    return "Bytea";
                case DbType.Currency:
                    return "money";
                case DbType.Time:
                case DbType.Date:
                case DbType.DateTime:
                    return "datetime";
                case DbType.Decimal:
                    return "numeric";
                case DbType.Double:
                    return "double";
                case DbType.Guid:
                    return "uuid";
                case DbType.UInt32:
                case DbType.UInt16:
                case DbType.Int16:
                case DbType.Int32:
                    return "integer";
                case DbType.UInt64:
                case DbType.Int64:
                    return "Bigint";
                case DbType.Single:
                    return "real";
                case DbType.VarNumeric:
                    return "numeric";
                case DbType.Xml:
                    return "Xml";
                default:
                    return "nvarchar";
            }
        }
    }
}
